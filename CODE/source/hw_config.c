/*
 * hw_config.c
 *
 *  Created on: 26 ����. 2016 �.
 *      Author: SV
 */

#include "91x_lib.h"
#include "91x_type.h"
#include "usb_lib.h"
#include "usb_prop.h"
#include "hw_config.h"
#include "usb_mem.h"
#include "usb_pwr.h"
#include "binary.h"
#include <intrinsics.h>

GPIO_InitTypeDef GPIO_InitStructure;
UART_InitTypeDef UART_InitStructure;
ADC_InitTypeDef ADC_InitStructure;
TIM_InitTypeDef TIM_InitStructure;

extern u32 count_out;
extern u8 buffer_out[];
extern u8 buffer_in[];
extern u32 count_in;

extern void Delay(u32 Delay);

/*******************************************************************************/

void Set_System(void)
{
    SCU_MCLKSourceConfig(SCU_MCLK_OSC);
    FMI_Config(FMI_READ_WAIT_STATE_2, FMI_WRITE_WAIT_STATE_0, FMI_PWD_ENABLE,
    FMI_LVD_ENABLE, FMI_FREQ_HIGH);
    SCU_PLLFactorsConfig(192, 25, 2);
    SCU_PLLCmd(ENABLE);
    SCU_MCLKSourceConfig(SCU_MCLK_PLL);

    SCU_AHBPeriphClockConfig(__VIC, ENABLE);
    SCU_AHBPeriphReset(__VIC, DISABLE);
    /*USB clock = MCLK= 48MHz*/
    SCU_USBCLKConfig(SCU_USBCLK_MCLK2);
    /*Enable USB clock*/
    SCU_AHBPeriphClockConfig(__USB, ENABLE);
    SCU_AHBPeriphReset(__USB, DISABLE);

    SCU_AHBPeriphClockConfig(__USB48M, ENABLE);

    SCU_APBPeriphClockConfig(__ADC, ENABLE);
    SCU_AHBPeriphReset(__ADC, DISABLE);

    /* Enable the UART1 Clock GSM Module */
    SCU_APBPeriphClockConfig(__UART1, ENABLE);
    SCU_AHBPeriphReset(__UART1, DISABLE);

    /* Enable the UART2 Clock GPS Module */
    SCU_APBPeriphClockConfig(__UART2, ENABLE);
    SCU_AHBPeriphReset(__UART2, DISABLE);

    SCU_APBPeriphClockConfig(__RTC, ENABLE);
    SCU_APBPeriphReset(__RTC, DISABLE);

    SCU_APBPeriphClockConfig(__TIM01, ENABLE);
    SCU_APBPeriphReset(__TIM01, DISABLE);

    SCU_APBPeriphClockConfig(__TIM23, ENABLE);
    SCU_APBPeriphReset(__TIM23, DISABLE);

    SCU_APBPeriphClockConfig(__SSP1, ENABLE);
    SCU_APBPeriphReset(__SSP1, DISABLE);

    /* Enable the GPIO0 Clock */
    SCU_APBPeriphClockConfig(__GPIO0, ENABLE);
    SCU_APBPeriphReset(__GPIO0, DISABLE);
    /* Enable the GPIO1 Clock */
    SCU_APBPeriphClockConfig(__GPIO1, ENABLE);
    SCU_AHBPeriphReset(__GPIO1, DISABLE);
    /* Enable the GPIO2 Clock */
    SCU_APBPeriphClockConfig(__GPIO2, ENABLE);
    SCU_AHBPeriphReset(__GPIO2, DISABLE);
    /* Enable the GPIO3 Clock */
    SCU_APBPeriphClockConfig(__GPIO3, ENABLE);
    SCU_AHBPeriphReset(__GPIO3, DISABLE);
    /* Enable the GPIO4 Clock */
    SCU_APBPeriphClockConfig(__GPIO4, ENABLE);
    SCU_AHBPeriphReset(__GPIO4, DISABLE);
    /* Enable the GPIO5 Clock */
    SCU_APBPeriphClockConfig(__GPIO5, ENABLE);
    SCU_AHBPeriphReset(__GPIO5, DISABLE);
    /* Enable the GPIO6 Clock */
    SCU_APBPeriphClockConfig(__GPIO6, ENABLE);
    SCU_AHBPeriphReset(__GPIO6, DISABLE);
    /* Enable the GPIO7 Clock */
    SCU_APBPeriphClockConfig(__GPIO7, ENABLE);
    SCU_AHBPeriphReset(__GPIO7, DISABLE);
    /* Enable the GPIO8 Clock */
    SCU_APBPeriphClockConfig(__GPIO8, ENABLE);
    SCU_AHBPeriphReset(__GPIO8, DISABLE);
    /* Enable the GPIO9 Clock */
    SCU_APBPeriphClockConfig(__GPIO9, ENABLE);
    SCU_APBPeriphReset(__GPIO9, DISABLE);

    /*Configure GPIO0 Output ( LED "CPU" on P0.6, LED "GPS" on P0.7)*/
    GPIO_DeInit(GPIO0);
    GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt1;
    GPIO_Init(GPIO0, &GPIO_InitStructure);
    /*GPIO0 Output all 0*/
    GPIO_Write(GPIO0, 0);

    GPIO_DeInit(GPIO1);
    /*Configure UART1_Tx pin GPIO1.0(SIM900)*/
    GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt2;
    GPIO_Init(GPIO1, &GPIO_InitStructure);

    /*Configure GPIO1.6 Output (CAN TX) */
    GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt1;
    GPIO_Init(GPIO1, &GPIO_InitStructure);

    /*Configure UART1_Rx pin GPIO1.1 and UART2_Rx pin GPIO1.3*/
    GPIO_InitStructure.GPIO_Direction = GPIO_PinInput;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_3;
    GPIO_InitStructure.GPIO_IPInputConnected = GPIO_IPInputConnected_Enable;
    GPIO_InitStructure.GPIO_Alternate = GPIO_InputAlt1;
    GPIO_Init(GPIO1, &GPIO_InitStructure);

    GPIO_DeInit(GPIO2);
    /*Configure GPIO2 Output (power SIM900 on P2.0) */
    GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt1;
    GPIO_Init(GPIO2, &GPIO_InitStructure);
    SIM900_Off;

    GPIO_DeInit(GPIO3);
    /*Configure UART2_Tx pin GPIO3.5(NEO6M)*/
    GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt3;
    GPIO_Init(GPIO3, &GPIO_InitStructure);

    /* Configure the GPIO4 pin 1 as analog input */
    GPIO_DeInit(GPIO4);
    GPIO_ANAPinConfig(GPIO_ANAChannel1, ENABLE);

    GPIO_DeInit(GPIO6);
    /*Configure GPIO6 Output (power NEO6 on P6.0) */
    GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt1;
    GPIO_Init(GPIO6, &GPIO_InitStructure);
    NEO6_Off;

    /*Configure GPIO6 Output (power key SIM900 on P6.3) */
    GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_OpenCollector;
    GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt1;
    GPIO_Init(GPIO6, &GPIO_InitStructure);
    SIM900_Key_Off;

    GPIO_DeInit(GPIO7);
    /*Configure GPIO7 Output (D+ Pull-Up on P7.6)*/
    GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt1;
    GPIO_Init(GPIO7, &GPIO_InitStructure);

    /*Configure GPIO9 Output (LED "GSM" on P9.0, LED "RF" on P9.1)*/
    GPIO_DeInit(GPIO9);
    GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt1;
    GPIO_Init(GPIO9, &GPIO_InitStructure);
    /*GPIO9 Output all 0*/
    GPIO_Write(GPIO9, 0);

    VIC_DeInit();
    VIC_InitDefaultVectors();

    /*Configure the UART1 and UART2*/
    UART_DeInit(UART1);  //GSM
    UART_InitStructure.UART_WordLength = UART_WordLength_8D;
    UART_InitStructure.UART_StopBits = UART_StopBits_1;
    UART_InitStructure.UART_Parity = UART_Parity_No;
    UART_InitStructure.UART_BaudRate = 19200;
    UART_InitStructure.UART_HardwareFlowControl = UART_HardwareFlowControl_None;
    UART_InitStructure.UART_Mode = UART_Mode_Tx_Rx;
    UART_InitStructure.UART_FIFO = UART_FIFO_Disable;

    UART_Init(UART1, &UART_InitStructure);
    UART_DMACmd(UART1, UART_DMAReq_Tx | UART_DMAReq_Rx, DISABLE);

    VIC_Config(UART1_ITLine, VIC_IRQ, 2);
    VIC_ITCmd(UART1_ITLine, ENABLE);

    UART_ITConfig(UART1, UART_IT_Transmit | UART_IT_Receive, ENABLE);
    UART_Cmd(UART1, ENABLE);

    UART_DeInit(UART2);  //GPS
    UART_InitStructure.UART_WordLength = UART_WordLength_8D;
    UART_InitStructure.UART_StopBits = UART_StopBits_1;
    UART_InitStructure.UART_Parity = UART_Parity_No;
    UART_InitStructure.UART_BaudRate = 9600;
    UART_InitStructure.UART_HardwareFlowControl = UART_HardwareFlowControl_None;
    UART_InitStructure.UART_Mode = UART_Mode_Tx_Rx;
    UART_InitStructure.UART_FIFO = UART_FIFO_Disable;

    UART_Init(UART2, &UART_InitStructure);
    UART_DMACmd(UART2, UART_DMAReq_Tx | UART_DMAReq_Rx, DISABLE);

    VIC_Config(UART2_ITLine, VIC_IRQ, 1);
    VIC_ITCmd(UART2_ITLine, ENABLE);

    UART_ITConfig(UART2, UART_IT_Receive, ENABLE);
    UART_Cmd(UART2, ENABLE);

    RTC_PeriodicIntConfig(RTC_Per_16Hz);
    RTC_ITConfig(RTC_IT_Per, ENABLE);
    VIC_Config(RTC_ITLine, VIC_IRQ, 3);
    VIC_ITCmd(RTC_ITLine, ENABLE);

    ADC_StructInit(&ADC_InitStructure);
    ADC_DeInit();
    ADC_InitStructure.ADC_Channel_1_Mode = ADC_NoThreshold_Conversion;
    ADC_InitStructure.ADC_Select_Channel = ADC_Channel_1;
    ADC_InitStructure.ADC_Scan_Mode = DISABLE;
    ADC_InitStructure.ADC_Conversion_Mode = ADC_Single_Mode;

    ADC_Cmd(ENABLE);
    ADC_PrescalerConfig(0x2);
    ADC_Init(&ADC_InitStructure);
    /*
     ADC_ITConfig(ADC_IT_ECV, ENABLE);
     VIC_Config(ADC_ITLine, VIC_IRQ, 5);
     VIC_ITCmd(ADC_ITLine, ENABLE);
     */

    /*******************************************************************************
     *
     *******************************************************************************/
    TIM_InitStructure.TIM_Mode = TIM_OCM_CHANNEL_1;
    TIM_InitStructure.TIM_OC1_Modes = TIM_TIMING;
    TIM_InitStructure.TIM_Clock_Source = TIM_CLK_APB;
    TIM_InitStructure.TIM_Prescaler = 96 - 1;
    TIM_InitStructure.TIM_Pulse_Length_1 = 1000;
    TIM_Init(TIM0, &TIM_InitStructure);

    // VIC configuration
    VIC_Config(TIM0_ITLine, VIC_IRQ, 2);
    VIC_ITCmd(TIM0_ITLine, ENABLE);

    /*******************************************************************************
     *
     *******************************************************************************/
    TIM_InitStructure.TIM_Mode = TIM_OCM_CHANNEL_1;
    TIM_InitStructure.TIM_OC1_Modes = TIM_TIMING;
    TIM_InitStructure.TIM_Clock_Source = TIM_CLK_APB;
    TIM_InitStructure.TIM_Prescaler = 96 - 1;
    TIM_InitStructure.TIM_Pulse_Length_1 = 0xfffb;
    TIM_Init(TIM1, &TIM_InitStructure);

    /*******************************************************************************
     *
     *******************************************************************************/
    TIM_InitStructure.TIM_Mode = TIM_OCM_CHANNEL_1;
    TIM_InitStructure.TIM_OC1_Modes = TIM_TIMING;
    TIM_InitStructure.TIM_Clock_Source = TIM_CLK_APB;
    TIM_InitStructure.TIM_Prescaler = 96 - 1;
    TIM_InitStructure.TIM_Pulse_Length_1 = 7777;
    TIM_Init(TIM2, &TIM_InitStructure);

    // VIC configuration
    VIC_Config(TIM2_ITLine, VIC_IRQ, 5);
    VIC_ITCmd(TIM2_ITLine, ENABLE);

}

/*******************************************************************************

 *******************************************************************************/
void USB_Cable_Config(FunctionalState NewState)
{
    if (NewState == ENABLE)
    {
	GPIO_WriteBit(GPIO7, GPIO_Pin_6, Bit_SET);
    }
    else
    {
	GPIO_WriteBit(GPIO7, GPIO_Pin_6, Bit_RESET);
    }
}
/*******************************************************************************

 *******************************************************************************/
void Enter_LowPowerMode(void)
{
    bDeviceState = SUSPENDED;
}
/*******************************************************************************

 *******************************************************************************/
void Leave_LowPowerMode(void)
{
    DEVICE_INFO *pInfo = &Device_Info;

    /* Set the device state to the correct state */
    if (pInfo->Current_Configuration != 0)
    {
	/* Device configured */
	bDeviceState = CONFIGURED;
    }
    else
    {
	bDeviceState = ATTACHED;
    }
}

void USB_Interrupts_Config(void)
{
    VIC_Config(USBLP_ITLine, VIC_IRQ, 0);
    VIC_ITCmd(USBLP_ITLine, ENABLE);
}

/*******************************************************************************

 *******************************************************************************/
void USB_To_UART_Send_Data(u8* data_buffer, u8 Nb_bytes)
{
    u32 i;

    for (i = 0; i < Nb_bytes; i++)
    {
	UART_SendData(UART1, *(data_buffer + i));
    }
}
/*******************************************************************************

 *******************************************************************************/
void USB_Send_Data(u8 data)   //TODO
{
    if (bDeviceState == 5)
    {
	if (count_in < 63)
	{
	    buffer_in[count_in] = data;
	    count_in++;
	}

    }
}
/*******************************************************************************

 *******************************************************************************/
void USB_Refr(void)
{
    if (bDeviceState == 5)
    {
	if (count_out)
	{
	    USB_To_UART_Send_Data(&buffer_out[0], count_out);
	    count_out = 0;
	}
	if (count_in)
	{
	    UserToPMABufferCopy(buffer_in, ENDP1_TXADDR, count_in);
	    SetEPTxCount(ENDP1, count_in);
	    SetEPTxValid(ENDP1);
	    count_in = 0;
	}
    }
}

/*************************************************
 * �������� ���������� � ������� ����
 *************************************************/
float checBortVoltage(void)
{
    ADC_ConversionCmd(ADC_Conversion_Start);
    for (int i = 0; i < 20; i++)
    {
	Delay(1);
	if (!ADC_GetFlagStatus(ADC_FLAG_ECV)) break;
    }
    ADC_ClearFlag(ADC_FLAG_ECV);
    return (58.9 * ADC_GetConversionValue(ADC_Channel_1)) / 1024;
}
/********************************END OF FILE**************************************/

