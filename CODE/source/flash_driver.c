/*
 * flash_driver.c
 *
 *  Created on: 4 ����� 2016 �.
 *      Author: SV
 */
#include "flash_driver.h"
#include <string.h>

FlashType flashType;
char dataBackup[100][160];
u8 dataCounter;

extern void Delay(u32 Delay);

/*******************************************************************************

 *******************************************************************************/
void FlashInit(void)
{
    SSP_InitTypeDef SSP_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;

    GPIO_DeInit(GPIO3);
    /* Configure SSP1_CLK, SSP1_MOSI */
    GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_6;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt2;
    GPIO_Init(GPIO3, &GPIO_InitStructure);

    /* Configure CS, HOLD pins */
    GPIO_InitStructure.GPIO_Direction = GPIO_PinOutput;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    GPIO_InitStructure.GPIO_Alternate = GPIO_OutputAlt1;
    GPIO_Init(GPIO3, &GPIO_InitStructure);

    GPIO_WriteBit(GPIO3, GPIO_Pin_3, Bit_SET);  // Hold ��� Reset
    GPIO_WriteBit(GPIO3, GPIO_Pin_2, Bit_SET);  // ChipSelect

    /* Configure SSP1_MISO pin GPIO1.2 */
    GPIO_InitStructure.GPIO_Direction = GPIO_PinInput;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
    GPIO_InitStructure.GPIO_Type = GPIO_Type_PushPull;
    GPIO_InitStructure.GPIO_IPInputConnected = GPIO_IPInputConnected_Enable;
    GPIO_InitStructure.GPIO_Alternate = GPIO_InputAlt1;
    GPIO_Init(GPIO1, &GPIO_InitStructure);

    /* SSP1 configuration */
    SSP_DeInit(SSP1);
    SSP_InitStructure.SSP_FrameFormat = SSP_FrameFormat_Motorola;
    SSP_InitStructure.SSP_Mode = SSP_Mode_Master;
    SSP_InitStructure.SSP_CPOL = SSP_CPOL_High;
    SSP_InitStructure.SSP_CPHA = SSP_CPHA_2Edge;
    SSP_InitStructure.SSP_DataSize = SSP_DataSize_8b;
    SSP_InitStructure.SSP_ClockRate = 0;
    SSP_InitStructure.SSP_ClockPrescaler = 2;
    SSP_Init(SSP1, &SSP_InitStructure);

    /* SSP1 enable */
    SSP_Cmd(SSP1, ENABLE);
}

/*********************************************************
* ����������� ���� flash ������
*********************************************************/
FlashType CheckFlashType(void)
{
    FlashInit();
    u8 byte5, byte6;
    SSP_FLASH_ChipSelect(Low);
    SSP_FLASH_SendByte(0x90);  // Manufacturer and Device ID Winbond
    SSP_FLASH_SendByte(0);
    SSP_FLASH_SendByte(0);
    SSP_FLASH_SendByte(0);
    byte5 = SSP_FLASH_SendByte(Dummy_Byte);
    byte6 = SSP_FLASH_SendByte(Dummy_Byte);
    SSP_FLASH_ChipSelect(High);
    if (byte5 == 0xEF & byte6 == 0x14) return WINBOND;
    GPIO_WriteBit(GPIO3, GPIO_Pin_3, Bit_RESET);  // ����� ��� ATMEL
    Delay(50);
    GPIO_WriteBit(GPIO3, GPIO_Pin_3, Bit_SET);
    Delay(50);
    SSP_FLASH_ChipSelect(Low);
    SSP_FLASH_SendByte(0x9F);  // Manufacturer and Device ID ATMEL
    byte5 = SSP_FLASH_SendByte(Dummy_Byte);
    byte6 = SSP_FLASH_SendByte(Dummy_Byte);
    SSP_FLASH_ChipSelect(High);
    if (byte5 == 0x1F & byte6 == 0x26) return ATMEL;
    return UNKNOWN;
}

/*******************************************************************************
* Erases the specified FLASH sector.
********************************************************************************/
void SSP_FLASH_SectorErase(u32 SectorAddr)
{
    SSP_FLASH_WriteEnable();

    SSP_FLASH_ChipSelect(Low);

    SSP_FLASH_SendByte(SE);

    SSP_FLASH_SendByte((SectorAddr & 0xFF0000) >> 16);

    SSP_FLASH_SendByte((SectorAddr & 0xFF00) >> 8);

    SSP_FLASH_SendByte(SectorAddr & 0xFF);

    SSP_FLASH_ChipSelect(High);

    SSP_FLASH_WaitForWriteEnd();
}

/*******************************************************************************
*
********************************************************************************/
void SSP_FLASH_BulkErase(void)
{
    SSP_FLASH_WriteEnable();

    SSP_FLASH_ChipSelect(Low);
    SSP_FLASH_SendByte(BE);
    SSP_FLASH_ChipSelect(High);

   // SSP_FLASH_WaitForWriteEnd(); XXX
}

/*******************************************************************************
 * Description    : Writes more than one byte to the FLASH with a single WRITE
 *                  cycle(Page WRITE sequence). The number of byte can't exceed
 *                  the FLASH page size.
 * Input          : - pBuffer : pointer to the buffer  containing the data to be
 *                    written to the FLASH.
 *                  - WriteAddr : FLASH's internal address to write to.
 *                  - NumByteToWrite : number of bytes to write to the FLASH,
 *                    must be equal or less than "SSP_FLASH_PageSize" value.
 *******************************************************************************/
void SSP_FLASH_PageWrite(u8* pBuffer, u32 WriteAddr, u16 NumByteToWrite)
{
    SSP_FLASH_WriteEnable();

    SSP_FLASH_ChipSelect(Low);

    SSP_FLASH_SendByte(WRITE);

    SSP_FLASH_SendByte((WriteAddr & 0xFF0000) >> 16);
    SSP_FLASH_SendByte((WriteAddr & 0xFF00) >> 8);
    SSP_FLASH_SendByte(WriteAddr & 0xFF);

    while (NumByteToWrite--)
    {
	SSP_FLASH_SendByte(*pBuffer);
	pBuffer++;
    }

    SSP_FLASH_ChipSelect(High);

    SSP_FLASH_WaitForWriteEnd();
}

/*******************************************************************************
 * Description    : Writes block of data to the FLASH. In this function, each
 *                  byte is written using Byte WRITE sequence.
 * Input          : - pBuffer : pointer to the buffer  containing the data to be
 *                    written to the FLASH.
 *                  - WriteAddr : FLASH's internal address to write to.
 *                  - NumByteToWrite : number of bytes to write to the FLASH.
 *******************************************************************************/
void SSP_FLASH_BlockWrite(u8* pBuffer, u32 WriteAddr, u16 NumByteToWrite)
{
    while (NumByteToWrite--)
    {
	SSP_FLASH_WriteEnable();

	SSP_FLASH_ChipSelect(Low);

	SSP_FLASH_SendByte(WRITE);
	SSP_FLASH_SendByte((WriteAddr & 0xFF0000) >> 16);
	SSP_FLASH_SendByte((WriteAddr & 0xFF00) >> 8);
	SSP_FLASH_SendByte(WriteAddr & 0xFF);

	SSP_FLASH_SendByte(*pBuffer);

	SSP_FLASH_ChipSelect(High);

	pBuffer++;

	WriteAddr++;

	SSP_FLASH_WaitForWriteEnd();
    }
}

/*******************************************************************************
 * Description    : Writes block of data to the FLASH. In this function, the
 *                  number of WRITE cycles are reduced, using Page WRITE sequence.
 * Input          : - pBuffer : pointer to the buffer  containing the data to be
 *                    written to the FLASH.
 *                  - WriteAddr : FLASH's internal address to write to.
 *                  - NumByteToWrite : number of bytes to write to the FLASH.
 *******************************************************************************/
void SSP_FLASH_BufferWrite(u8* pBuffer, u32 WriteAddr, u16 NumByteToWrite)
{
    u8 NumOfPage = 0, NumOfSingle = 0, Addr = 0, count = 0, temp = 0;

    Addr = WriteAddr % SSP_FLASH_PageSize;
    count = SSP_FLASH_PageSize - Addr;
    NumOfPage = NumByteToWrite / SSP_FLASH_PageSize;
    NumOfSingle = NumByteToWrite % SSP_FLASH_PageSize;

    if (Addr == 0) /* WriteAddr is SSP_FLASH_PageSize aligned  */
    {
	if (NumOfPage == 0) /* NumByteToWrite < SSP_FLASH_PageSize */
	{
	    SSP_FLASH_PageWrite(pBuffer, WriteAddr, NumByteToWrite);
	}
	else /* NumByteToWrite > SSP_FLASH_PageSize */
	{
	    while (NumOfPage--)
	    {
		SSP_FLASH_PageWrite(pBuffer, WriteAddr, SSP_FLASH_PageSize);
		WriteAddr += SSP_FLASH_PageSize;
		pBuffer += SSP_FLASH_PageSize;
	    }

	    SSP_FLASH_PageWrite(pBuffer, WriteAddr, NumOfSingle);
	}
    }
    else /* WriteAddr is not SSP_FLASH_PageSize aligned  */
    {
	if (NumOfPage == 0) /* NumByteToWrite < SSP_FLASH_PageSize */
	{
	    if (NumOfSingle > count) /* (NumByteToWrite + WriteAddr) > SSP_FLASH_PageSize */
	    {
		temp = NumOfSingle - count;

		SSP_FLASH_PageWrite(pBuffer, WriteAddr, count);
		WriteAddr += count;
		pBuffer += count;

		SSP_FLASH_PageWrite(pBuffer, WriteAddr, temp);
	    }
	    else
	    {
		SSP_FLASH_PageWrite(pBuffer, WriteAddr, NumByteToWrite);
	    }
	}
	else /* NumByteToWrite > SSP_FLASH_PageSize */
	{
	    NumByteToWrite -= count;
	    NumOfPage = NumByteToWrite / SSP_FLASH_PageSize;
	    NumOfSingle = NumByteToWrite % SSP_FLASH_PageSize;

	    SSP_FLASH_PageWrite(pBuffer, WriteAddr, count);
	    WriteAddr += count;
	    pBuffer += count;

	    while (NumOfPage--)
	    {
		SSP_FLASH_PageWrite(pBuffer, WriteAddr, SSP_FLASH_PageSize);
		WriteAddr += SSP_FLASH_PageSize;
		pBuffer += SSP_FLASH_PageSize;
	    }

	    if (NumOfSingle != 0)
	    {
		SSP_FLASH_PageWrite(pBuffer, WriteAddr, NumOfSingle);
	    }
	}
    }
}

/*******************************************************************************
 * Description    : Reads a block of data from the FLASH.
 * Input          : - pBuffer : pointer to the buffer that receives the data read
 *                    from the FLASH.
 *                  - ReadAddr : FLASH's internal address to read from.
 *                  - NumByteToRead : number of bytes to read from the FLASH.
 *******************************************************************************/
void SSP_FLASH_BufferRead(u8* pBuffer, u32 ReadAddr, u16 NumByteToRead)
{
    SSP_FLASH_ChipSelect(Low);

    SSP_FLASH_SendByte(READ);

    SSP_FLASH_SendByte((ReadAddr & 0xFF0000) >> 16);

    SSP_FLASH_SendByte((ReadAddr & 0xFF00) >> 8);

    SSP_FLASH_SendByte(ReadAddr & 0xFF);

    while (NumByteToRead--)
    {
	*pBuffer = SSP_FLASH_SendByte(Dummy_Byte);
	pBuffer++;
    }

    /* Deselect the FLASH: Chip Select high */
    SSP_FLASH_ChipSelect(High);
}

/*******************************************************************************
*
********************************************************************************/
static void SSP_FLASH_ChipSelect(u8 State)
{
    GPIO_WriteBit(GPIO3, GPIO_Pin_2, (BitAction) State);
}

/*******************************************************************************
 * Description    : Sends a byte through the SSP interface and return the  byte
 *                  received from the SSP bus.
 * Input          : byte : byte to send.
 * Return         : The value of the received byte.
 *******************************************************************************/
static u8 SSP_FLASH_SendByte(u8 byte)
{
    u8 i = 32;
    SSP1->DR = byte;

    while (SSP_GetFlagStatus(SSP1, SSP_FLAG_TxFifoEmpty) == RESET)
    {
	if (--i) continue;  // ��������� �� ���������
	break;
    }

    i = 32;
    while (SSP_GetFlagStatus(SSP1, SSP_FLAG_RxFifoNotEmpty) == RESET)
    {
	if (--i) continue;  // ��������� �� ���������
	break;
    }

    return (SSP1->DR);
}

/*******************************************************************************

 *******************************************************************************/
static void SSP_FLASH_WriteEnable(void)
{
    SSP_FLASH_ChipSelect(Low);

    SSP_FLASH_SendByte(WREN);

    SSP_FLASH_ChipSelect(High);
}

/*******************************************************************************

 *******************************************************************************/
static void SSP_FLASH_WaitForWriteEnd(void)
{
    u8 FLASH_Status = 0;

    SSP_FLASH_ChipSelect(Low);

    SSP_FLASH_SendByte(RDSR);

    do
    {

	FLASH_Status = SSP_FLASH_SendByte(Dummy_Byte);
	
    } while ((FLASH_Status & WIP_Flag) == SET);

    SSP_FLASH_ChipSelect(High);
}

/***************************************************************
 * ��������� ������ � ������
 ***************************************************************/
void SaveData(const char *str)
{
    switch (flashType)
    {
	case ATMEL:
	    strcpy(dataBackup[dataCounter++], str); //TODO
	break;

	case WINBOND:
	    strcpy(dataBackup[dataCounter++], str); //TODO
	break;

	default:
	    strcpy(dataBackup[dataCounter++], str);
	break;
    }
}

/***************************************************************
 * ��������� ������ �� ������
 ***************************************************************/
char* LoadData()
{
    switch (flashType)
    {
	case ATMEL:
	    return dataBackup[dataCounter]; //TODO

	case WINBOND:
	    return dataBackup[dataCounter]; //TODO

	default:
	    return dataBackup[dataCounter];
    }
}
