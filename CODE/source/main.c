/********************************************************
 * main.c
 *
 *  Created on: 2 ����. 2016 �.
 *      Author: SV
 ********************************************************/
#include "flash_driver.h"
#include "91x_lib.h"
#include "91x_type.h"
#include "usb_lib.h"
#include "usb_conf.h"
#include "usb_prop.h"
#include "usb_pwr.h"
#include "hw_config.h"
#include "gsm.h"
#include "flash_driver.h"
#include <stdio.h>
#include <string.h>
#include <intrinsics.h>
#include <stdlib.h>

u32 fuelLevel; // ������� �������
GsmStatus gsmStat = GsmNotAnsver; //��������� ������
DataState gpsStat = NotReady; //���� ��������� ������ �� GPS
DataState timeStat = NotReady; //���� ��������� RTC
static int motionAngle = 0; // ����������� ��������

extern FlashType flashType; // ��� ���������� ������
char statCode[6];  // ������ ������ ����
extern float bortVoltage;
extern GPSDataType gpsData;
extern char tmpData[160];
extern char localTime[7];
extern char localDate[7];
extern char latitudeGSM[12];
extern char longitudeGSM[12];
extern char gsmIMEI[17];
extern u8 dataCounter;

extern void Delay(u32 Delay);
extern u32 FreqCounter(void);
extern GsmStatus GsmStart(void);
extern void GsmStop(void);
extern GsmStatus GsmConnectInternet(void);
extern ErrorStatus GsmSendData(const char *str);
extern ErrorStatus CellidLocation(void);
extern FlashType CheckFlashType(void);
extern void SSP_FLASH_BulkErase(void);
extern void GetLocalDate(void);
extern void GetLocalTime(void);
extern void SetLocalTime(void);
extern void SaveData(const char *str);
extern char* LoadData();
extern float checBortVoltage(void);

int main()
{
    static bool dataSend = TRUE; // ��������� �� ������������� �������� ������
    static ErrorStatus errStat;
    static u32 i;
    static u32 save;
    static char tmpLatitudeGSM[12];
    static char tmpCode[6];
    static u8 counter = 253;
#ifdef DEBUG
    debug();
#endif
    Set_System();
    USB_Init();
    LedCPU_Blink;
    Delay(5);
    flashType = CheckFlashType();
    SSP_FLASH_BulkErase(); //XXX
    NEO6_On;
    Delay(500);
    // Enable TIM2 counter
    TIM_ITConfig(TIM2, TIM_IT_OC1, ENABLE);
    TIM_CounterCmd(TIM2, TIM_START);
    gsmStat = GsmStart();
    if (gsmStat == GsmReady) gsmStat = GsmConnectInternet();
    CAN_On;
    strcpy(statCode, SysPowerOn);
    strcpy(tmpCode, Speed_0);
    while (1)
    {
	if (counter++ > 11)
	{
	    bortVoltage = checBortVoltage();  // �������� ���������� �������� ����
	    UART_Cmd(UART2, DISABLE);
	    fuelLevel = FreqCounter();  // �������� ������� �������
	    UART_Cmd(UART2, ENABLE);
	    counter = 0;
	}
	if (gsmStat != GsmConnected)  // ���������� ������
	{
	    GsmStop();
	    gsmStat = GsmStart();
	}
	if (gsmStat == GsmReady) gsmStat = GsmConnectInternet();
	i = 0;
	while (gpsStat != Valid)
	{
	    Delay(1000);
	    if (++i > 600) break; // ���� ��������� GPS 10 ���.
	}
	if (gpsStat == Valid)   // ���� ������ �� GPS ����������
	{
	    if (dataSend == TRUE)  // ���� ���������� ��������
	    {
		if (gsmStat == GsmConnected)
		{
		    StringForm(tmpData);
		    errStat = GsmSendData(tmpData);
		    if (strcmp(statCode, SysPowerOn) == 0) strcpy(statCode, Speed_0);
		}
		else errStat = ERROR;
		save = __get_interrupt_state();
		__disable_interrupt();
		gpsStat = NotValid;
		__set_interrupt_state(save);
		dataSend = FALSE;
		if (errStat == ERROR)
		{
		    gsmStat = GsmReady;
		    SaveData(tmpData);
		}
		continue;
	    }
	    if (timeStat != Valid) SetLocalTime();
	    int tmpSpeed = atoi(gpsData.groundSpeed);  // ��������
	    int tmpAngle = atoi(gpsData.speedAngle);   // �����������
	    int delta = abs(tmpAngle - motionAngle);
	    if (delta > 180) delta = 360 - delta;
	    if (delta > 15)
	    {
		dataSend = TRUE;
		motionAngle = tmpAngle;
	    }
	    if (tmpSpeed <= 1) strcpy(tmpCode, Speed_0);

	    else if (tmpSpeed > 3 && (strcmp(statCode, Speed_0) == 0)) strcpy(tmpCode, Speed_5);

	    else if (tmpSpeed >= 60 && tmpSpeed < 70) strcpy(tmpCode, Speed_60);

	    else if (tmpSpeed >= 70 && tmpSpeed < 90) strcpy(tmpCode, Speed_70);

	    else if (tmpSpeed >= 90 && tmpSpeed < 110) strcpy(tmpCode, Speed_90);

	    else if (tmpSpeed >= 110 && tmpSpeed < 130) strcpy(tmpCode, Speed_110);

	    else if (tmpSpeed >= 130) strcpy(tmpCode, Speed_130);

	    if (strcmp(statCode, tmpCode))
	    {
		strcpy(statCode, tmpCode);
		dataSend = TRUE;
	    }
	    if (dataSend == TRUE) continue;
	    if ((gsmStat == GsmConnected) && dataCounter)   // ���� ���� �� ������������ ������ - ����������
	    {
		errStat = GsmSendData(LoadData());
		if (errStat == SUCCESS) dataCounter--;
	    }
	    save = __get_interrupt_state();
	    __disable_interrupt();
	    gpsStat = NotValid;
	    __set_interrupt_state(save);
	}
	else   // ���� ��� GPS ������
	{
	    if (gsmStat == GsmConnected)
	    {
		errStat = CellidLocation();
		if (errStat == SUCCESS)
		{
		    if (strcmp(statCode, GPSFAILURE)) strcpy(tmpLatitudeGSM, "000");
		    if (!strcmp(latitudeGSM, tmpLatitudeGSM)) continue; // ���� ���� �� ����������
		    strcpy(tmpLatitudeGSM, latitudeGSM);
		    if (timeStat == Valid)
		    {
			GetLocalDate();
			GetLocalTime();
		    }
		    else
		    {
			strcpy(localDate, "000000");
			strcpy(localTime, "000000");
		    }
		    strcpy(statCode, GPSFAILURE);
		    sprintf(tmpData, "GET /gprmc/data?acct=admin&dev=%15s&lat=%10s&lon=%10s&mph=%s&dir=%3s"
			    "&time=%6s&date=%6s&alt=%u&batv=%.1f&code=%5s", gsmIMEI, latitudeGSM, longitudeGSM, "000",
			    "000", localTime, localDate, fuelLevel, bortVoltage, statCode);
		    GsmSendData(tmpData);
		}
	    }
	}
    }
}
/****************************************END OF FILE********************************************/
