/*
 * systick.c
 *
 *  Created on: 4 ����� 2016 �.
 *      Author: SV
 */

#include "91x_lib.h"
#include "systick.h"
#include <intrinsics.h>
#include <stdio.h>
#include <string.h>
#include "gps.h"

volatile u8 blinkFlag;
volatile u8 ledCPUBlink = DISABLE;
volatile u8 ledGPSBlink = DISABLE;
volatile u32 dlyCount;
char localTime[7];
char localDate[7];
extern DataState gpsStat;
extern GPSDataType gpsData;
extern DataState timeStat;

void LedBlink(void)
{
    blinkFlag ^= 0x1;
    if (ledCPUBlink)
    {
	GPIO_WriteBit(GPIO0, GPIO_Pin_6, ((BitAction) blinkFlag));
    }
    if (ledGPSBlink)
    {
	GPIO_WriteBit(GPIO0, GPIO_Pin_7, ((BitAction) blinkFlag));
    }
}

/******************************
 * Delay
 ******************************/
void Tim0Handler(void)
{
    // Clear TIM0 counter
    TIM_CounterCmd(TIM0, TIM_CLEAR);
    if (dlyCount)
    {
	--dlyCount;
    }
    // Clear TIM0 flag OC1
    TIM_ClearFlag(TIM0, TIM_FLAG_OC1);
}

/******************************
 * �������� � ������������
 ******************************/
void Delay(u32 delay)
{

    dlyCount = delay;

    // Clear TIM0 counter
    TIM_CounterCmd(TIM0, TIM_CLEAR);
    // Clear TIM0 flag OC1
    TIM_ClearFlag(TIM0, TIM_FLAG_OC1);
    // Enable TIM0 OC1 interrupt
    TIM_ITConfig(TIM0, TIM_IT_OC1, ENABLE);
    // Enable TIM0 counter
    TIM_CounterCmd(TIM0, TIM_START);

    while (dlyCount);

    // Disable TIM0 OC1 interrupt
    TIM_ITConfig(TIM0, TIM_IT_OC1, DISABLE);
    // Disable TIM0 counter
    TIM_CounterCmd(TIM0, TIM_STOP);
}

/************************************
 * �������� ������� �� �1.5
 ************************************/
u32 FreqCounter(void)

{
    u8 flag;
    u16 counter;
    u32 save;
    // Clear TIM1 counter
    TIM_CounterCmd(TIM1, TIM_CLEAR);
    // Clear TIM1 flag OC1
    TIM_ClearFlag(TIM1, TIM_FLAG_OC1);
    save = __get_interrupt_state();
    __disable_interrupt();
    flag = GPIO_ReadBit(GPIO1, GPIO_Pin_5);
    // Enable TIM1 counter
    TIM_CounterCmd(TIM1, TIM_START);
    while (flag == GPIO_ReadBit(GPIO1, GPIO_Pin_5))
    {
	if (TIM_GetFlagStatus(TIM1, TIM_FLAG_OC1))
	{
	    __set_interrupt_state(save);
	    return 1;
	}
    }
    // Clear TIM1 counter
    TIM_CounterCmd(TIM1, TIM_CLEAR);

    while (flag != GPIO_ReadBit(GPIO1, GPIO_Pin_5))
    {
	if (TIM_GetFlagStatus(TIM1, TIM_FLAG_OC1))
	{
	    __set_interrupt_state(save);
	    return 0;
	}
    }

    while (flag == GPIO_ReadBit(GPIO1, GPIO_Pin_5))
    {
	if (TIM_GetFlagStatus(TIM1, TIM_FLAG_OC1))
	{
	    __set_interrupt_state(save);
	    return 0;
	}
    }

    // Disable TIM1 counter
    TIM_CounterCmd(TIM1, TIM_STOP);
    __set_interrupt_state(save);
    counter = TIM_GetCounterValue(TIM1);
    if (counter) return 1000000 / counter;
    return 0;
}

/******************************************************
 * �������� ����� �� RTC � localTime
 *****************************************************/
void GetLocalTime(void)
{
    RTC_TIME Time;
    RTC_GetTime(BINARY, &Time);
    sprintf(localTime, "%.2u%.2u%.2u", Time.hours, Time.minutes, Time.seconds);
}

/******************************************************
 * �������� ���� �� RTC � localDate
 *****************************************************/
void GetLocalDate(void)
{
    RTC_DATE Date;
    RTC_GetDate(BINARY, &Date);
    sprintf(localDate, "%.2u%.2u%.2u", Date.day, Date.month, Date.year);
}

/***************************************************************
 * ���������� ����� � ���� � RTC �� GPS. ���� �� ������� � �������
 * 2 ������, ���������� ERROR, � ��������� ������ SUCCESS
 ***************************************************************/
void SetLocalTime(void)
{
    u8 i = 0;
    RTC_TIME Time;
    RTC_DATE Date;
    u32 save;

    save = __get_interrupt_state();
    __disable_interrupt();
    gpsStat = NotValid;
    __set_interrupt_state(save);
    while (gpsStat != Valid)
    {
	Delay(10);
	if (++i == 200) return;
    }
    sscanf(gpsData.time, "%2hhd%2hhd%2hhd", &Time.hours, &Time.minutes, &Time.seconds);
    RTC_SetTime(Time);
    sscanf(gpsData.date, "%2hhd%2hhd%2hhd", &Date.day, &Date.month, &Date.year);
    Date.century = 20;
    RTC_SetDate(Date);
    timeStat = Valid;
}
