/*
 * gps.c
 *
 *  Created on: 26 ����. 2016 �.
 *      Author: SV
 */
#include "gps.h"
#include "91x_lib.h"
#include "91x_type.h"
#include "hw_config.h"

static u8 numToken = 0;  //����� ��������� � ������
static u8 numInToken = 0;  //����� ������� � ���������
static ParserStateType parserState;
static u8 tempGPS[12][12];  // ������ ��� ���������� ���������� ������ GPS

extern DataState gpsStat; //���� ��������� ������ �� GPS
GPSDataType gpsData;

void ParsingGPS(char data)  // ������ ������ GPS
{
    u8 i = 0;
    if (gpsStat == Valid) return;
    if (data == '$')
    {
	numInToken = 0;
	numToken = 0;
	parserState = S_State;
	return;
    }
    switch (parserState)
	{
	case S_State: // ����� ������ $GPRMC
	    switch (numInToken)
		{
		case 0:
		    if (data != 'G')
		    {
			parserState = Free;
			return;
		    }
		    numInToken++;
		    break;

		case 1:
		    if (data != 'P')
		    {
			parserState = Free;
			return;
		    }
		    numInToken++;
		    break;

		case 2:
		    if (data != 'R')
		    {
			parserState = Free;
			return;
		    }
		    numInToken++;
		    break;

		case 3:
		    if (data != 'M')
		    {
			parserState = Free;
			return;
		    }
		    numInToken++;
		    break;

		case 4:
		    if (data != 'C')
		    {
			parserState = Free;
			return;
		    }
		    numInToken++;
		    break;

		case 5:
		    numInToken = 0;
		    numToken = 0;
		    if (data != ',')
		    {
			parserState = Free;
			return;
		    }
		    parserState = GPRMC_State;
		    break;

		default:
		    parserState = Free;
		    break;
		}
	    break;

	case GPRMC_State: // ������ ������ GPRMC
	    switch (data)
		{
		case ',':  // ��������� ��������

		    if ((numToken > 10) || (numInToken > 11))
		    {
			parserState = Free;
			break;
		    }
		    tempGPS[numToken][numInToken] = 0;
		    numInToken = 0;
		    numToken++;
		    break;

		case '*':  // ������ ��������
		    if ((numToken == 11) && (numInToken <= 11))
		    {
			tempGPS[numToken][numInToken] = 0;
			if (tempGPS[1][0] == 'A') //���� ������ ����������
			{
			    for (i = 0; i < 6; i++)
				gpsData.time[i] = tempGPS[0][i];
			    gpsData.time[6] = 0;

			    for (i = 0; i < 9; i++)   //TODO
			    {
				gpsData.latitude[i] = tempGPS[2][i];
			    }
			    gpsData.latitude[9] = 0;

			    for (i = 0; i < 10; i++)  //TODO
			    {
				gpsData.longitude[i] = tempGPS[4][i];
			    }
			    gpsData.longitude[10] = 0;

			    for (i = 0; i < 4; i++)
			    {
				gpsData.groundSpeed[i] = tempGPS[6][i];
				if (gpsData.groundSpeed[i] == '.')
				{
				    gpsData.groundSpeed[i] = 0;
				    break;
				}
			    }
			    if ((tempGPS[7][0] == 0) || (tempGPS[7][0] == '.'))
			    {
				gpsData.speedAngle[0] = '0';
				gpsData.speedAngle[1] = 0;
			    }
			    else
			    {
				for (i = 0; i < 4; i++)
				{
				    gpsData.speedAngle[i] = tempGPS[7][i];
				    if (gpsData.speedAngle[i] == '.')
				    {
					gpsData.speedAngle[i] = 0;
					break;
				    }
				}
			    }

			    for (i = 0; i < 6; ++i)
				gpsData.date[i] = tempGPS[8][i];
			    gpsData.date[6] = 0;

			    gpsStat = Valid;
			    LedGPS_Blink;
			    LedRF_Off;
			}

			else
			{
			    gpsStat = NotValid;
			    LedGPS_Off
			    ;
			    LedRF_On;
			}
		    }
		    parserState = Free;
		    break;

		default: // ������ �������� �������
		    if (numInToken > 10)
		    {
			parserState = Free;
			break;
		    }
		    tempGPS[numToken][numInToken] = data;
		    numInToken++;
		    break;
		}

	    break;

	case Free:
	    break;

	default:
	    parserState = Free;
	    break;
	}
}

