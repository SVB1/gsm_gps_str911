/*
 * gsm.c
 *
 *  Created on: 9 ����� 2016 �.
 *      Author: SV
 */

#include <gsm.h>

extern GPSDataType gpsData;
extern u32 fuelLevel;

float bortVoltage;
char gsmIMEI[17];
char line[250];
char tmpData[160];
char celLoc[70];
char latitudeGSM[12];
char longitudeGSM[12];
extern char statCode[6];
u32 timeOut = 6000;

// ����� ������
char rx_buffer_usart1[RX_BUFFER_SIZE_USART1];

#if RX_BUFFER_SIZE_USART1 <= 256
unsigned char rx_wr_index_usart1 = 0, rx_rd_index_usart1 = 0;
#else
unsigned int rx_wr_index_usart1=0,rx_rd_index_usart1=0;
#endif

#if RX_BUFFER_SIZE_USART1 < 256
unsigned char rx_counter_usart1 = 0;
#else
unsigned int rx_counter_usart1=0;
#endif

// ���� ������������
char rx_buffer_overflow_usart1 = 0;

// USARTC1 Receiver interrupt service routine
void usart1_rx_isr(u8 data)
{
    rx_buffer_usart1[rx_wr_index_usart1++] = data;
#if RX_BUFFER_SIZE_USART1 == 256
    // special case for receiver buffer size=256
    if (++rx_counter_usart1 == 0) rx_buffer_overflow_usart1=1;
#else
    if (rx_wr_index_usart1 == RX_BUFFER_SIZE_USART1) rx_wr_index_usart1 = 0;
    if (++rx_counter_usart1 == RX_BUFFER_SIZE_USART1)
    {
	rx_counter_usart1 = 0;
	rx_buffer_overflow_usart1 = 1;
    }
#endif
}

// ���������� 'getchar' function

GETCHAR_PROTO
{
    u32 getCharTimeOut; //�������: ����� timeOut/100 ������ getchar ������ EOF
    char data;
    u32 save;

    getCharTimeOut = timeOut;
    while (rx_counter_usart1 == 0)
    {
	Delay(10);
	if (!getCharTimeOut--)
	{
	    return EOF;
	}
    }
    data = rx_buffer_usart1[rx_rd_index_usart1++];
#if RX_BUFFER_SIZE_USART1 != 256
    if (rx_rd_index_usart1 == RX_BUFFER_SIZE_USART1) rx_rd_index_usart1 = 0;
#endif
    save = __get_interrupt_state();
    __disable_interrupt();
    --rx_counter_usart1;
    __set_interrupt_state(save);
    return data;
}

/*************************************************************
 * ������� ��������� ������
 *************************************************************/
void RxBuferFlush(void)
{
    rx_rd_index_usart1 = 0;
    rx_wr_index_usart1 = 0;
    rx_counter_usart1 = 0;
    rx_buffer_overflow_usart1 = 0;
}

// ����� ��������

u8 tx_buffer_usart1[TX_BUFFER_SIZE_USART1];

#if TX_BUFFER_SIZE_USART1 <= 256
u8 tx_wr_index_usart1 = 0, tx_rd_index_usart1 = 0;
#else
unsigned int tx_wr_index_usart1=0,tx_rd_index_usart1=0;
#endif

#if TX_BUFFER_SIZE_USART1 < 256
u8 tx_counter_usart1 = 0;
#else
unsigned int tx_counter_usart1=0;
#endif

// USART1 Transmitter interrupt service routine
void usart1_tx_isr(void)
{
    if (tx_counter_usart1)
    {
	--tx_counter_usart1;
	UART_SendData(UART1, tx_buffer_usart1[tx_rd_index_usart1++]);
#if TX_BUFFER_SIZE_USART1 != 256
	if (tx_rd_index_usart1 == TX_BUFFER_SIZE_USART1) tx_rd_index_usart1 = 0;
#endif
    }
}

// ���������� 'putchar' function

PUTCHAR_PROTO
{
    u32 save;
//    USB_Send_Data(ch);  //XXX
    while (tx_counter_usart1 == TX_BUFFER_SIZE_USART1);
    save = __get_interrupt_state();
    __disable_interrupt();
    if (tx_counter_usart1 || (UART_GetFlagStatus(UART1, UART_FLAG_TxFIFOFull) != RESET))
    {
	tx_buffer_usart1[tx_wr_index_usart1++] = (u8) ch;
#if TX_BUFFER_SIZE_USART1 != 256
	if (tx_wr_index_usart1 == TX_BUFFER_SIZE_USART1) tx_wr_index_usart1 = 0;
#endif
	++tx_counter_usart1;
    }
    else UART_SendData(UART1, ch);
    __set_interrupt_state(save);
    return ch;
}

/*************************************************
 * ������������� ������
 ************************************************/
GsmStatus GsmStart(void)
{
    SIM900_On;
    Delay(100);
    SIM900_Key_On;
    LedGSM_Off;
    Delay(700);
    SIM900_Key_Off;
    LedGSM_On;

    do
    {
	if (gets(line)) continue;
	return GsmNotAnsver;       //����� �� ��������
    } while (strcmp(line, "RDY\r") != 0); //���� ����������

    do
    {
	if (gets(line)) continue;
	return GsmNotAnsver;       //����� �� ��������
    } while (strcmp(line, "+CPIN: READY\r") != 0); //���� ������

    do
    {
	if (gets(line)) continue;
	return GsmNotLink;       //��� ����
    } while (strcmp(line, "+CREG: 1\r") != 0); //���� ������

    do
    {
	if (gets(line)) continue;
	return GsmNotLink;       //��� ����
    } while (strcmp(line, "+CGREG: 1\r") != 0); //���� ������

    if (gsmIMEI[0] == 0)  // IMEI SIM900
    {
	printf("AT+GSN\r");
	do
	{
	    if (gets(line)) continue;
	    return GsmNotAnsver;
	} while (strcmp(line, "AT+GSN\r\r") != 0);

	gets(gsmIMEI);

	do
	{
	    if (gets(line)) continue;
	    break;
	} while (strcmp(line, "OK\r") != 0);

	for (int i = 0; i < sizeof(gsmIMEI); ++i)  //��������� \0 � ����� ������
	{
	    if (gsmIMEI[i] == '\r')
	    {
		gsmIMEI[i] = '\0';
		break;
	    }
	}
    }
    return GsmReady;   //������������� ������ �������
}

/*********************************************************
 * ���������� ������
 *********************************************************/
void GsmStop(void)
{
    SIM900_Key_On;
    LedGSM_Off;
    Delay(1000);
    SIM900_Key_Off;
    Delay(5000);
}

/*************************************************
 * ����������� � ���������
 ************************************************/
GsmStatus GsmConnectInternet(void)
{
    printf("AT + CSTT=\"internet.mts.ru\",\"mts\",\"mts\"\r");
    do
    {
	if (gets(line)) continue;
	return GsmReady;       //��� ����������� � ���������
    } while (strcmp(line, "OK\r") != 0);

    timeOut = 18000;
    printf("AT+CIICR\r");
    do
    {
	if (gets(line)) continue;
	return GsmReady;       //��� ����������� � ���������
    } while (strcmp(line, "OK\r") != 0);
    timeOut = 6000;

    printf("AT+CIFSR\r");
    if (!gets(line)) return GsmReady;

    return GsmConnected;
}

/**********************************************
 * ������������ ������ ��� ��������
 **********************************************/
void StringForm(char *str)
{
    sprintf(str, "GET /gprmc/data?acct=admin&dev=%15s&lat=%9s&lon=%10s&mph=%s&dir=%s&time=%6s&date=%6s"
	    "&alt=%05u&batv=00%.1f&code=%5s", gsmIMEI, gpsData.latitude, gpsData.longitude, gpsData.groundSpeed,
	    gpsData.speedAngle, gpsData.time, gpsData.date, fuelLevel, bortVoltage, statCode);
}

/*************************************************
 * �������� ������
 ************************************************/
ErrorStatus GsmSendData(const char *str)
{
    char s;
    LedGSM_Off;
    printf("AT+CIPSTART=\"TCP\",\"85.235.55.150\",\"8080\"\r");
    do
    {
	if (gets(line)) continue;
	return ERROR;
    } while (strcmp(line, "CONNECT OK\r") != 0);
    LedGSM_On;
    printf("AT+CIPSEND\r");
    do
    {
	if (gets(line)) continue;
	return ERROR;
    } while (strcmp(line, "AT+CIPSEND\r\r") != 0);
    s = getchar();
    if (s != '>') return ERROR;
    printf(str);
    CrLf;
    CtrlZ;
    CrLf;
    do
    {
	if (gets(line)) continue;
	return ERROR;
    } while (strcmp(line, "SEND OK\r") != 0);
    LedGSM_Off;
    Delay(300);
    LedGSM_On;
    printf("AT+CIPCLOSE=1\r");
    Delay(100);
    return SUCCESS;
}

/*****************************************************
 * ������ ��������������
 *****************************************************/
ErrorStatus CellidLocation(void)
{
    char s;
    u32 countryCode;
    u32 operatorId;
    u32 lac;
    u32 cellId;

    RxBuferFlush();
    printf("AT+CENG=1\r");
    do
    {
	if (gets(line)) continue;
	return ERROR;
    } while (strcmp(line, "OK\r") != 0);
    printf("AT+CENG?\r");
    do
    {
	if (gets(line)) continue;
	return ERROR;
    } while (strcmp(line, "+CENG: 1,0\r") != 0);
    Delay(500);
    if (!(gets(line))) return ERROR;
    if (!(gets(line))) return ERROR;
    sscanf(line, "+CENG:0,\"%*d,%*d,%*d,%d,%d,%*d,%x,%*d,%*d,%x,%*d\"", &countryCode, &operatorId, &cellId, &lac);
    do
    {
	if (gets(line)) continue;
	return ERROR;
    } while (strcmp(line, "OK\r") != 0);
    printf("AT+CENG=0\r");
    do
    {
	if (gets(line)) continue;
	return ERROR;
    } while (strcmp(line, "OK\r") != 0);

    printf("AT+CIPSTART=\"TCP\",\"213.180.204.138\",\"80\"\r");
    do
    {
	if (gets(line)) continue;
	return ERROR;
    } while (strcmp(line, "CONNECT OK\r") != 0);

    printf("AT+CIPSEND\r");
    do
    {
	if (gets(line)) continue;
	return ERROR;
    } while (strcmp(line, "AT+CIPSEND\r\r") != 0);
    s = getchar();
    if (s != '>') return ERROR;
    printf("GET http://mobile.maps.yandex.net/cellid_location/?countrycode="
	    "%u&operatorid=%u&lac=%u&cellid=%u", countryCode, operatorId, lac, cellId);
    CrLf
    ;
    CtrlZ;
    CrLf
    ;
    do
    {
	if (gets(line)) continue;
	return ERROR;
    } while (strcmp(line, "SEND OK\r") != 0);
    RxBuferFlush();
    if (!(gets(line))) return ERROR;
    if (!(gets(line))) return ERROR;
    if (!strncmp(line, "<error", 6)) return ERROR;
    if (!(gets(celLoc))) return ERROR;
    if (!(gets(line))) return ERROR;
    printf("AT+CIPCLOSE=1\r");
    Delay(2500);
    sscanf(celLoc, " <coordinates latitude=\"%[^\"] \" longitude=\"%[^\"]", &latitudeGSM, &longitudeGSM);
    return SUCCESS;
}
