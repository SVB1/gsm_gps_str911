/*
 * systic.h
 *
 *  Created on: 4 ����� 2016 �.
 *      Author: SV
 */

#ifndef _SYSTIC_H_
#define _SYSTIC_H_

void LedBlink(void);
void Delay(u32 Delay);
void Tim0Handler (void);
void Tim1Handler (void);
void GetLocalDate(void);
void GetLocalTime(void);
void SetLocalTime(void);


#endif /* CODE_INCLUDE_SYSTIC_H_ */
