/********************************************************
 * hw_config.h
 *
 *  Created on: 26 ����. 2016 �.
 *      Author: SV
 ********************************************************/

#ifndef __HW_Config_H
#define __HW_Config_H

extern u8 ledCPUBlink;
extern u8 ledGPSBlink;

#define CAN_On GPIO_WriteBit(GPIO0, GPIO_Pin_5, Bit_SET)
#define CAN_Off GPIO_WriteBit(GPIO0, GPIO_Pin_5, Bit_RESET)
#define LedCPU_On GPIO_WriteBit(GPIO0, GPIO_Pin_6, Bit_SET)
#define LedCPU_Off {ledCPUBlink = DISABLE; GPIO_WriteBit(GPIO0, GPIO_Pin_6, Bit_RESET)}
#define LedCPU_Blink ledCPUBlink = ENABLE
#define LedGPS_On GPIO_WriteBit(GPIO0, GPIO_Pin_7, Bit_SET)
#define LedGPS_Off ledGPSBlink = DISABLE; GPIO_WriteBit(GPIO0, GPIO_Pin_7, Bit_RESET)
#define LedGPS_Blink ledGPSBlink = ENABLE
#define LedGSM_On GPIO_WriteBit(GPIO9, GPIO_Pin_0, Bit_SET)
#define LedGSM_Off GPIO_WriteBit(GPIO9, GPIO_Pin_0, Bit_RESET)
#define LedRF_On GPIO_WriteBit(GPIO9, GPIO_Pin_1, Bit_SET)
#define LedRF_Off GPIO_WriteBit(GPIO9, GPIO_Pin_1, Bit_RESET)
#define SIM900_On GPIO_WriteBit(GPIO2, GPIO_Pin_0, Bit_SET)
#define SIM900_Off GPIO_WriteBit(GPIO2, GPIO_Pin_0, Bit_RESET)
#define SIM900_Key_On GPIO_WriteBit(GPIO6, GPIO_Pin_3, Bit_RESET)
#define SIM900_Key_Off GPIO_WriteBit(GPIO6, GPIO_Pin_3, Bit_SET)
#define NEO6_Off GPIO_WriteBit(GPIO6, GPIO_Pin_0, Bit_SET)
#define NEO6_On GPIO_WriteBit(GPIO6, GPIO_Pin_0, Bit_RESET)

void Set_System(void);
void USB_Cable_Config (FunctionalState NewState);
void Enter_LowPowerMode(void);
void Leave_LowPowerMode(void);
void USB_Interrupts_Config(void);
void USB_To_UART_Send_Data(u8* data_buffer, u8 Nb_bytes);
void USB_Send_Data(u8 data);
void USB_Refr(void);
float checBortVoltage(void);
#endif /* __HW_CONFIG_H */
/**************************************END OF FILE*********************************************/
