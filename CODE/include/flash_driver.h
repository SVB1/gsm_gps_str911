/********************************************************
 * flash_driver.h
 *
 *  Created on: 26 ����. 2016 �.
 *      Author: SV
 ********************************************************/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SSP_FLASH_DRIVER_H
#define __SSP_FLASH_DRIVER_H

/* Includes ------------------------------------------------------------------*/
#include "91x_lib.h"
/* Private typedef -----------------------------------------------------------*/
#define SSP_FLASH_PageSize 256

#define WRITE      0x02  /* Write to Memory instruction */
#define WRSR       0x01  /* Write Status Register instruction */
#define WREN       0x06  /* Write enable instruction */

#define READ       0x03  /* Read from Memory instruction */
#define RDSR       0x05  /* Read Status Register instruction  */
#define SE         0xD8  /* Sector Erase instruction */
#define BE         0xC7  /* Bulk Erase instruction */

#define Low        0x00  /* ChipSelect line low */
#define High       0x01  /* ChipSelect line high */

#define WIP_Flag   0x01  /* Write In Progress (WIP) flag */

#define Dummy_Byte 0xA5

void FlashInit(void);
void SSP_FLASH_SectorErase(u32 SectorAddr);
void SSP_FLASH_BulkErase(void);
void SSP_FLASH_PageWrite(u8* pBuffer, u32 WriteAddr, u16 NumByteToWrite);
void SSP_FLASH_BlockWrite(u8* pBuffer, u32 WriteAddr, u16 NumByteToWrite);
void SSP_FLASH_BufferWrite(u8* pBuffer, u32 WriteAddr, u16 NumByteToWrite);
void SSP_FLASH_BufferRead(u8* pBuffer, u32 ReadAddr, u16 NumByteToRead);
static void SSP_FLASH_ChipSelect(u8 State);
static u8 SSP_FLASH_SendByte(u8 byte);
static void SSP_FLASH_WriteEnable(void);
static void SSP_FLASH_WaitForWriteEnd(void);
void SaveData(const char *str);
char* LoadData();


#endif /* __SSP_FLASH_DRIVER_H */

/***************************END OF FILE******************************************/
