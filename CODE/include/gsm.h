/*
 * gsm.h
 *
 *  Created on: 9 ����� 2016 �.
 *      Author: SV
 */
#include "91x_type.h"
#include "91x_lib.h"
#include "91x_type.h"
#include "systick.h"
#include <intrinsics.h>
#include <stdio.h>
#include <string.h>
#include "hw_config.h"
#include "gps.h"

#ifndef _UARTGSM_H_
#define _UARTGSM_H_

#define RX_BUFFER_SIZE_USART1 255
#define TX_BUFFER_SIZE_USART1 255

#define GETCHAR_PROTO int getchar(void)
#define PUTCHAR_PROTO int putchar(int ch)

#define CrLf putchar(13); putchar(10)
#define CtrlZ putchar(26)
#define Cr putchar(13)

/* ���� ��������� ��� �������*/
#define SysPowerOn "64793"
#define IgnitionOff "61719"
#define IgnitionOn "61718"
#define Speed_0 "61715"
#define Speed_5 "61713"
#define Speed_60 "61792"
#define Speed_70 "61793"
#define Speed_90 "61794"
#define Speed_110 "61795"
#define Speed_130 "61796"
#define GPSFAILURE "64802"


void usart1_rx_isr(u8 data);
void usart1_tx_isr(void);
GsmStatus GsmStart(void);
GsmStatus GsmConnectInternet(void);
ErrorStatus GsmSendData(const char *str);
void RxBuferFlush(void);
ErrorStatus CellidLocation(void);
void StringForm(char *str);
void GsmStop(void);

#endif /* _UARTGSM_H_ */
