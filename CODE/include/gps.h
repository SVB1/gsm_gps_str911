/*
 * gps.h
 *
 *  Created on: 26 ����. 2016 �.
 *      Author: SV
 */

#ifndef _GPS_H_
#define _GPS_H_

#include "91x_type.h"

typedef enum
    {
    Free,
    S_State,
    GPRMC_State
    } ParserStateType;   //������ ��������� GPS ������

/********************************************************
*��������� ���. ��� ���������� ������ GPS
*��������� � �������� ���������, � ����� ���� � �����
********************************************************/
    typedef struct
        {
    	char time[11];  // "hhmmss" �� �����
    	char latitude[11];  //������ "xxxx.xxxxx"
    	char longitude[11];  //������� "xxxxx.xxxx"
    	char groundSpeed[11];  //�������� �� �����
    	char speedAngle[11];  //����������� �� �����
    	char date[11];  // "DDMMYY"

        } GPSDataType;

void ParsingGPS(char data);

#endif /* _GPS_H_ */
