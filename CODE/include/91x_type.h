/*
 * 91x_type.h
 *
 *  Created on: 26 ����. 2016 �.
 *      Author: SV
 */

#ifndef __91x_type_H
#define __91x_type_H

  typedef long long                u64;
  typedef unsigned long            u32;
  typedef unsigned short           u16;
  typedef unsigned char            u8;

  typedef signed long              s32;
  typedef signed short             s16;
  typedef signed char              s8;

  typedef volatile unsigned long   vu32;
  typedef volatile unsigned short  vu16;
  typedef volatile unsigned char   vu8;

  typedef volatile signed long     vs32;
  typedef volatile signed short    vs16;
  typedef volatile signed char     vs8;

typedef enum { FALSE = 0, TRUE} bool;

typedef enum { RESET = 0, SET} FlagStatus, ITStatus;

typedef enum { DISABLE = 0, ENABLE} FunctionalState;

typedef enum { ERROR = 0, SUCCESS} ErrorStatus;

typedef enum { GsmNotAnsver = 0, GsmNotLink, GsmReady, GsmConnected} GsmStatus; //��������� ������ GSM

typedef enum { UNKNOWN = 0, WINBOND, ATMEL} FlashType;

typedef enum {NotReady, Valid, NotValid} DataState;

#endif /* __91x_type_H */

/**********************************END OF FILE****************************************/
